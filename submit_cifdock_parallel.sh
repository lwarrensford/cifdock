#!/bin/bash

### ARP Jan 2023
### cluster version
### specify protein name, protein conformation number, and ligand
### specify CHARMM path


####   1.   SET UP   ####


### 1a. set up variables

protein=${1}
prot_conf=${2}
ligand=${3}
charmm=/shares/cas.chem.hlw/hlw/chem/charmm/c41a1/exec/em64t-qchem-Mon.12.28.2015/charmm
mkdir vars

wdir=`pwd`
pldir=${wdir}/${protein}_${prot_conf}_${ligand}
nlconf=`head -n 1 ligands/${ligand}/number_of_ligand_conf`   
vars=${wdir}/vars/var_${protein}_${prot_conf}_${ligand}

### 1b. record variables 

rm ${vars}
touch ${vars}
echo "protein="${protein} >> ${vars}
echo "prot_conf="${prot_conf} >> ${vars}
echo "ligand="${ligand} >> ${vars}
echo "nlconf="${nlconf} >> ${vars}
echo "pldir="${pldir} >> ${vars}
echo "charmm="${charmm} >> ${vars}
echo "vars="${vars} >> ${vars}

### 1c. prepare directory

mkdir ${pldir}
cp ${vars} ${pldir} 
cp -r cifdocking/* ${pldir}

cd ${pldir}
mkdir joblog/ scoring/ energy/

### 1d. submit slurm script

sbatch cifdock_parallel.slu ${vars}
