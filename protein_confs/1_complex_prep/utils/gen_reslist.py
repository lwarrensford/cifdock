"""
Generates a stream file that defines which residues will be mutated to 
alanine during the CIFDock procedure. Requires both residue numbers 
and residue types inputted from the user. Protein ID is kept for 
record-keeping only.

-LCW 6.16.22
"""

from datetime import datetime

mutants = {}
mutants["resnum"] = []
mutants["restype"] = []
now = datetime.now()

print("Welcome! Please input your protein ID followed by all residues "
	+ "\nyou'd like to include. After a residue number is inputted, "
	+ "\nthe system will ask you to input the type of residue. When you "
	+ "\nare finished, type 'done' to generate the stream file.")

protein = input("\nPlease input your protein ID: ")

while True:
	# Get residue numbers and type from user, quit when 'done' is typed
	curr_resn = input("Please input next residue: ")
	if curr_resn.isnumeric():
		mutants["resnum"].append(curr_resn)
		curr_rest = input("Next please input the type of residue: ")
		mutants["restype"].append(curr_rest)
	elif curr_resn == "done":
		break
	else:
		print("Invalid residue number")

# Write stream file in CHARMM format
f = open("residue_list.str", 'w')
f.write("* Residue mutation selection file for protein " + protein)
f.write("\n*\n")
f.write("\nSET NPOS = " + str(len(mutants["resnum"])) + "\n")

# Format the residue list as CHARMM variables for mutation/backmutation
for i in range(len(mutants["resnum"])):
	f.write("\nSET RD" + str(i+1) + "    = " + 
		str(mutants["resnum"][i]).strip("[']"))
	f.write("\nSET SEGID" + str(i+1) + " = PROA")
	f.write("\nSET RES" + str(i+1) + "   = " + 
		str(mutants["restype"][i]).strip("[']").upper())
	f.write("\n")

print("residue_list.str file generated, have a nice day!")
f.write("\n\n! Mutation selection generated on " + 
	now.strftime("%m/%d/%Y %H:%M:%S"))
f.close()
