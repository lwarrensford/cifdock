"""
Generate a stream file that defines the active site of the target 
protein. Requires input from the user on which residues to include.
The last inputted entry can be removed with an undo command.
Protein ID is kept for record-keeping only.

-LCW 6.16.22
"""

from datetime import datetime

residues = []
max_len = 0
now = datetime.now()

print("Welcome! Please input your protein ID followed by all residues "
	+ "\nyou'd like to include. When you're finished, type 'done' to "
	+ "\ngenerate the stream file. If you'd like to remove the last entry,"
	+ "\ntype 'undo' to remove it.")

protein = input("\nPlease input your protein ID: ")

while True:
	# Collect user-inputted residues, quit when 'done' is typed
	curr_res = input("Please input next residue: ")
	if curr_res.isnumeric():
		residues.append(curr_res)
	elif curr_res == "undo":
		residues.remove(residues[-1])
	elif curr_res == "done":
		break
	else:
		print("Invalid residue number")

# Write the stream file in CHARMM format
f = open("origin.str", 'w')
f.write("* Origin selection file for protein " + protein)
f.write("\n*\n")
f.write("\ndefine origin select segid PROA .and. ( ")

# Format the residue list as a CHARMM selection
for res in residues:
	if res == residues[-1]:
		f.write("resid " + str(res) + " ) show end")
		f.write("\n\n! Residue selection generated on " + 
			now.strftime("%m/%d/%Y %H:%M:%S"))
		f.close()
		print("origin.str file generated, have a nice day!")
	elif max_len == 4:
		f.write("-\n")
		max_len = 0
	else:
		f.write("resid " + str(res) + " .or. ")
		max_len += 1

