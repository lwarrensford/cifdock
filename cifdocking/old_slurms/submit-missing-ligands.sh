#!bin/bash
## Submit any missing ligands

export script=${1}
export vars=${2}

source ${vars}
cat ${vars}

while read p
do

lig_num=${p}

sbatch ${script}.slu ${lig_num} ${vars}

done < missing.dat
