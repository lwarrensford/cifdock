#!/bin/bash

### Specify protein name, the protein conformation number, and the number of subjobs you want running at one time (batch size)
### Be sure to account for multiple ligands so you don't go over your batch limit
### This is designed for one protein conformation, but you can incorporate more by adding another while loop
### nlconf is the number of ligand conformations per ligand


# Initialize protein variables, batch size, directories 
export protein=${1}
export prot_conf=${2}
export batch=${3}
export wdir=`pwd`

mkdir vars

while read p
do
 
  # Get total number of ligand conformations
  nlconf=`head -n 1 ligands/${p}/number_of_ligand_conf`
 
  # Save out variables
  export pldir=${wdir}/${protein}_${prot_conf}_${p}
  export vars=${wdir}/vars/var_${protein}_${prot_conf}_${p}
  touch ${vars}
 
  echo "export protein="${protein} >> ${vars}
  echo "export prot_conf="${prot_conf} >> ${vars}
  echo "export ligand="${p} >> ${vars}
  echo "export batch="${batch} >> ${vars}
  echo "export nlconf="${nlconf} >> ${vars}
  echo "export pldir="${pldir} >> ${vars}
  echo "export vars="${vars} >> ${vars}
 
  # Organize SLURM outputs 
  mkdir -p ${pldir}/slurm
  cp ${vars} ${pldir}
  cd ${pldir}
  cp -r ${wdir}/cifdock-2.0/* .
  mkdir energy/ scoring/
  
  # Save out the ligand conformation numbers
  for (( i=1; i<=$nlconf; i+=1 ))
  do
    echo ${i} >> complete_start_ligands.dat
  done
  
  # Submit SGLD submission scripts
  for (( lig_num=1; lig_num<=$batch; lig_num+=1 ))
  do
    sbatch sgld.slu ${lig_num} ${vars} 
  done
  
  cd ${wdir}
 
done < liglist.dat


### end
