#!/bin/bash
# Identifies which conformations failed so that they can
# be resubmitted


memlimit=${1}000000
vars=$2

source ${vars}
mkdir slurm/failed
rm temp2.dat
rm temp3.dat
touch temp2.dat
touch temp3.dat
touch slurm-info.dat

# Ones that failed due to exceeded memory
grep "exceeded memory" slurm-* > temp.dat
echo "   *****  Cancelled due to Memory Limit  ***** " >> slurm_info.dat
while read p
do

mem=`echo $p | awk '{ print $8 }' | grep -o '[0-9]\+'`
slurmnum=`echo $p | awk '{ print $1 }' | grep -o '[0-9]\+'`
oldn=`grep "N <-" slurm-${slurmnum}.out | grep -o '[0-9]\+'`

if [[ "$mem" -lt "$memlimit" ]]
then
  echo $oldn >> temp2.dat
  echo $oldn "  " $mem "  "$slurmnum >> slurm_info.dat
else
  newn=$((oldn+batch))
  if [[ "$newn" -le "$total_conf" ]] 
  then
     echo ${newn} >> temp2.dat
     echo $oldn"-->"$newn "  " $mem "  "$slurmnum >> slurm_info.dat
  else
##     newn=$((newn-total_conf))
##     echo ${newn} >> temp3.dat
##     echo $oldn"-->"$newn "  " $mem "  "$slurmnum >> slurm_info.dat
     echo "skipped:" $oldn "  " $mem "  " $slurmnum >> slurm_info.dat  
  fi
fi

mv slurm-${slurmnum}.out slurm/failed
done < temp.dat

## Now we'll do the time limit ones
rm temp.dat
echo " " >> slurm_info.dat
echo " " >> slurm_info.dat
echo " " >> slurm_info.dat
echo "  *****  Cancelled due to Time Limit  *****   " >> slurm_info.dat

grep "TIME LIMIT" slurm-* > temp.dat

while read p
do
slurmnum=`echo $p | awk '{ print $1 }' | grep -o '[0-9]\+'`
oldn=`grep "N <-" slurm-${slurmnum}.out | grep -o '[0-9]\+'`
echo $oldn "  " $slurmnum >> temp2.dat
 
echo $oldn "  "$slurmnum >> slurm_info.dat
mv slurm-${slurmnum}.out slurm/failed
done < temp.dat

## Now we'll put it all on one line to make it nice and easy to resubmit

rm new-N.dat

echo "   *****  submit current script  ******" >> new-N.dat
echo "         " >> new-N.dat
awk 'BEGIN { ORS=" " }; { print $1 }' temp2.dat >> new-N.dat

##echo "         " >> new-N.dat
##echo "         " >> new-N.dat

##echo "   *****  submit next script  ******" >> new-N.dat
##echo "         " >> new-N.dat
##awk 'BEGIN { ORS=" " }; { print $1 }' temp3.dat >> new-N.dat
##echo "         " >> new-N.dat
rm temp*.dat

# Uncomment this if you'd like it to automatically submit the failed ones

current=`head -n 3 new-N.dat | tail -n 1` 
##next=`tail -n 1 new-N.dat`

for i in ${current}
do
  echo $i
  sbatch backmutate.slu $i $vars
done

##for i in ${next}
##do
##  echo $i
##  sbatch trajectory.slu $i $vars
##done

