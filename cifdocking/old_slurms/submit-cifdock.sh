#!/bin/bash

## Specify protein name, the protein conformation number, and the number of subjobs you want running at one time (batch size)
## Be sure to account for multiple ligands so you don't go over your batch limit
## This is designed for one protein conformation, but you can incorporate more by adding another while loop
## nlconf is the number of ligand conformations per ligand
 
# Initialize protein variables, batch size, directories
protein=${1}
prot_conf=${2}
ligand=${3}
batch=${4}
wdir=`pwd`

mkdir vars

nlconf=`head -n 1 ligands/${ligand}/number_of_ligand_conf`

pldir=${wdir}/${protein}_${prot_conf}_${ligand}

vars=${wdir}/vars/var_${protein}_${prot_conf}_${ligand}
rm ${vars}
touch ${vars}

echo "protein="${protein} >> ${vars}
echo "prot_conf="${prot_conf} >> ${vars}
echo "ligand="${ligand} >> ${vars}
echo "batch="${batch} >> ${vars}
echo "nlconf="${nlconf} >> ${vars}
echo "pldir="${pldir} >> ${vars}
echo "vars="${vars} >> ${vars}

# Organize SLURM outputs
mkdir -p ${pldir}/slurm
cp ${vars} ${pldir}
cd ${pldir}
cp -r ${wdir}/cifdocking/* .

# Get number of ligand conformations
for (( i=1; i<=$nlconf; i+=1 ))
do
  echo ${i} >> complete_start_ligands.dat
done

# Submit SGLD submissions scripts
for (( lig_num=1; lig_num<=$batch; lig_num+=1 ))
do
  sbatch sgld.slu ${lig_num} ${vars} 
done

cd ${wdir}




