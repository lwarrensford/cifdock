**Note:** Submission procedures have been updated. CIFDock now uses GNU Parallel to submit subjobs. Old submission scripts can be found in old_slurms.

#### How to Submit

- Submit the docking to the cluster from the main directory
  `sh submit_cifdock_parallel.sh [protein] [protein conformation number] [ligand]`
   - protein:
     - the name of the protein target
   - protein conformation number:
     - this is your protein conformation number. Make sure that it matches whatever is in your "protein_confs" folder
   - ligand:
     - the name of the ligand that will be docked
   - example
     - `sh submit-cifdock.sh 2acr 1 tol`
       - will submit the protein 2ACR, conformation number 1, ligand tolrestat (tol)

#### Organization 

1. variable file
   - a directory called vars will be created per protein-ligand combo. In order, this includes:
	- protein name
        - protein conformation number
        - ligand name
        - batch number
        - initial number of ligand poses
        - protein directory path
        - vars file path
        - number of conformations after clustering (generated during cluster2.slu)
   - this file is necessary for these scripts to work properly. Avoid editing if necessary. 
   - this and the ligand conformation number are the only things passed through to the next script
        - this allows you to restart anywhere in the process without having to pass through a 
          ton of variables
   - a copy of this will be in the protein ligand directory, but the file the scripts use is in
      the vars directory

2. checking the progress of jobs
   - as the jobs run, they write the ligand number to a file called complete_[script-name] (i.e., complete_backmutate)
   - gnu parallel will create a joblog/ directory with a job log file for each subjob (e.g., 9_sgld, 10_cluster1, etc.)
   - open a job log (e.g., 9_sgld.log) to view subjob status and run times
    - exit values other than 0 indicate a failed subjob

#### Troubleshooting 

**Note:** Deprecated. Use only if using the old submission procedure from previous builds.

1. submit-from-start.sh
   - if you need to start a submission script from the beginning:
   	- example: if cluster2.slu finished, but did not submit the next submission script 
   - you will need your script name and your vars path name
	- get your vars path name by opening it's vars file and copying vars=path name
   - submitting:
	`sh submit-from-start.sh [script-name] [vars-path]`
	- example:
		`sh submit-from-start.sh backmutate /work/n/name/2acr/vars/var_2acr_1_tol`
2. submit-missing-ligands.sh
   - use this if some ligands don't complete for some reason
   - it's recommend you check your slurm output to see if there's an easy reason for why the specific ligands
     died (remember to use joblist.dat)
   - so here is where those complete_[script-name] files come in handy. If the number in the complete_[script-name].dat 
     doesn't match its respective complete_ligands.dat, then you can use sdiff -s to find the ligand numbers
     that didn't succeed. You can then save these numbers into a file called missing.dat
   - submitting:
	`sh submit-missing-ligands.sh [script-name] [vars-path]`
   	- example:
		`sdiff -s complete_end_ligands.dat complete_backmutate.dat > missing.dat`
 		`sh submit-missing-ligands.sh backmutate /work/n/name/tmpr/vars/var_2acr_1_tol`

#### Script Breakdown 

1. submit-cifdock.sh
   - submits one protein conformation at a time
   - will use the "batch" number to submit the first loop of ligand conformations
     to the cluster for each ligand.

2. sgld.slu
   - will conduct sgld sims and loop through the rest of the conformations. Each submission is
     independent of the other jobs running (e.g., job 1 finishes --> submits job 11 --> submits job 21)
   - when the loop has reached the number of ligand configurations, will launch the next script 
     (e.g., max number is 200, so when it reaches 201, will submit cluster1.slu with 1 as the conf number)

3. cluster1.slu
   - first round of clustering
   - will loop like the previous. Instead of resubmitting in a loop,
     will check an output file and wait until everything is finished and then launch cluster2.slu

4. cluster2.slu
   - will finish clustering as one job after cluster1 is fully complete
   - once complete, will launch the next set in the same way submit-cif-dock does with the original batch

5. backmutate, trajectory, and energy will loop through the numbers until done in the same way that 
   sgld.slu does 

6. scoring.slu
   - will score the poses as one job after energy.slu is complete

