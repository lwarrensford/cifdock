"""
Python script that goes along with my CHARMM RMS script. This coordinates the various inputs needed
and will run the required CHARMM scripts and organizes the output RMSDs

Luke Warrensford 2019
"""

import os
import os.path
import glob
import time


pdbs = []

# Collects the protein/ligand IDs from the user
ref = input("ID of your reference protein: ")
ligand = input("ID of your docking ligand: ")

# Collects the pose numbers from the list of filenames
for filename in glob.glob('backmutate/pdbbackmutate_mini*.psf'):
    if len(filename) == 34:
        pdbs.append(str(filename[-5:-4]))
    elif len(filename) == 35:
        pdbs.append(str(filename[-6:-4]))
    elif len(filename) == 36:
        pdbs.append(str(filename[-7:-4]))
    else:
        continue

print("The following poses have been identified and their RMSDs will be calculated:")
print(pdbs)

RMSs =[]
os.system('touch final_rmsd.dat')
first_rms = False

# Main loop that runs the CHARMM RMS script
for pdb in pdbs:
    os.system('$CHARMM pose=' + pdb + ' ref=' + ref + ' ligand=' + ligand + ' -i rms.inp -o rms_' + pdb + '.dat')
    time.sleep(5)
    with open('rms_'+ pdb +'.dat') as readOutput:
        oLines = readOutput.readlines()
        for line in range(0, len(oLines)):
            oLines[line] = str(oLines[line])
            oLine = oLines[line].split()
            try:
                if oLine[-1] == "record":
                    first_rms = True
                else:
                    pass
            except IndexError:
                continue
            try:
                if oLine[0] == "THUS" and first_rms:
                    RMSs.append(str(oLine[4]))
                    with open('final_rmsd.dat', 'a') as a:
                        a.write('\nRMSD value for pose ' + pdb + ' is : ' + str(oLine[4]))
                        first_rms = False
                else:
                    continue
            except IndexError:
                continue
        print(RMSs)

# Sorts the RMSDs by lowest RMSD first
os.system('sort -nk8,8 final_rmsd.dat > sorted_final_rmsd.dat')
