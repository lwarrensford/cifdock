The following programs require installation prior to use of the 
CIFDock workflow:

#### CHARMM

The free version of CHARMM (charmm) is available for all academic, government, 
and nonprofit use. It is required for all steps of the CIFDock workflow and must be 
installed both on the local machine (for interactive jobs) as well as the computing 
cluster you plan to use CIFDock on (for the docking procedure). Information on 
obtaining and installing CHARMM can be found on the official [website](https://www.charmm.org)

#### OpenBabel (obabel) with Confab

Obabel is required to set up a ligand for CIFDock. Please ensure that 
your obabel version includes Confab (v2.4 or higher), otherwise the confab commands 
will not run correctly. Additionally, no error is shown if you try to run confab on 
an unsupported version. If this is the case, the first thing you'll want to try is 
checking your obabel version and installing an updated version if necessary. Info 
on obtaining and installing obabel can be found on the official [website](http://openbabel.org/wiki/Main_Page)

No other dependencies required! The CIFDock workflow itself consists of a series of 
input scripts that are either run interactively or on a cluster. Therefore, there is 
no official "installation" of CIFDock. Scripts can be used immediately upon download. 
