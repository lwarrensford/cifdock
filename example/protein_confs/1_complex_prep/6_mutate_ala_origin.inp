* Creating a charmm coor and psf file after mutating some activesite residues to ALA
*

dimension chsize 1500000

prnlev 5
wrnlev 5
bomlev -2

! Read topology and parameter files
read rtf card name ../../toppar/top_all36_prot.rtf
read rtf card append name ../../toppar/top_all36_cgenff.rtf

read para card flex name ../../toppar/par_all36_prot.prm
read para card append flex name ../../toppar/par_all36_cgenff.prm

stream ../../toppar/toppar_water_ions.str
stream ../../toppar/patch.str

open unit 12 form read name activesite_origin_@pdbid.psf 
read psf card unit 12
close unit 12

open unit 13 card read name activesite_origin_@pdbid.cor 
read coor card unit 13
close unit 13

! If you wish to define the origin you can do so by moving the native ligand to
! the centroid (the whole complex moves with the ligand translation too)
! However, defining the active-site residues and moving them to the centroid
! would be more efficient here.
! An easy way to do this is to view the original pdb file in vmd and display all
! residue around the native ligand.

! This stream defines the residues in the binding site
stream origin.str

! Here we are getting the centroid value for the defined active-site residue selection
! in origin.str
coor statistics select ( origin ) end
energy

! Now move the active site centroid to the origin
coordinate translate xdirection -?XAVE ydirection -?YAVE zdirection -?ZAVE
coor statistics select ( origin ) end


set bbselfull = (type C .or. type CA -
       .or. type N .or. type O .or. type H  .or. type HN -
       .or. type CB .or. type HA .or. type HB*)

! Here we get the first and last residue numbers in the cor file and set the following variables

system "cat a-pro-protein.cor | grep -i "ca" | grep -i "a-pro" |  head -n 1 | awk '{print $2}' > residue_num"

system "cat a-pro-protein.cor | grep -i "ca" | grep -i "a-pro" |  tail -n 1 | awk '{print $2}' >> residue_num" 

open unit 14 read form name "residue_num"
get terminus1 unit 14
increment terminus1 by 0
get terminus_last unit 14
increment terminus_last by 0


! Here the residue_list.str will contain info on which residues will be mutated to ala
stream residue_list.str 
set rpn = 0

label nextala
      incr rpn by 1
      if  @rpn  gt @npos then goto print_pdb_init
      !position to change and its segment identifier
      set resnum = @rd@@rpn
      set segid = @segid@@rpn

      set isnterm = false
      set iscterm = false
      !calc terminus1 =  1
      if resnum eq @terminus1 set isnterm = true
      !calc terminus_last =  0 + 468
      if resnum eq @{terminus_last} set iscterm = true


      dele atom SELE SEGID @segid .AND. RESID @resnum .AND. .not. @bbselfull end

      ! c) PATCH ALA 
      if @isnterm eq false  then if @iscterm eq false PATCH PALA @segid  @resnum SETUP
      if @isnterm eq true  PATCH NALA @segid  @resnum SETUP
      if @iscterm eq true  PATCH CALA @segid  @resnum SETUP
      rename resname ALA sele SEGID @segid .AND. resid @resnum end

      coor INIT SELE SEGID @segid .AND. RESID @resnum .AND. .NOT. @bbselfull end

      ic purge
      ic para
      ic build


      goto nextala

label print_pdb_init
define missing select .not. hydrogen .and. .not. initialized end
set missingatoms = ?nsel

print coor select missing end
cons fix sele none end

ic fill preserve
ic param
ic build

hbuild

cons fix sele none end

! Delete native ligand
! Only necessary if you have a co-crystallized ligand
! If your native ligand is a co-factor, comment out 
! this command so that your co-factor is not deleted
delete atom selec segid A-BAD show end

write coor pdb name mutala_origin_protein.pdb
* Initial structures with designed positions
* mutated to ala.

write coor card name mutala_origin_ protein.cor
* Initial structures with designed positions
* mutated to ala.

write psf card name mutala_origin_protein.psf
* Initial structures with designed positions
* mutated to ala.

! Save out water coordinates for later
delete atom sele all .and. .not. (resnm TIP3 .or. resnm POT .or. resnm CLA) show end

write coor pdb name waters_init.pdb
* Initial xtal waters
*

write coor card name waters_init.cor
* Initial xtal waters
*

write psf card name waters_init.psf
* Initial xtal waters
*




stop

