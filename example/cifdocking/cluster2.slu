#!/bin/bash
## Script 4 of workflow. submits 11_cluster2.inp and 12_final-clustering.inp

#SBATCH -J cluster2
#SBATCH -t 00:10:00
#SBATCH -n 1

# Initialize variables
vars=${1}
cluster2_slurm=${pldir}/slurm/cluster2
jobid=${SLURM_JOB_ID}
CHARMM=/shares/cas.chem.hlw/hlw/chem/charmm/c41a1/exec/em64t-qchem-Mon.12.28.2015/charmm

source ${vars}
cat ${vars}

mkdir ${cluster2_slurm}

echo ${protein}" " ${ligand} 
echo "nlconf is "$nlconf

# Initialize clustering variables for charmm
# Set the group variable higher if you would like to 
# cluster more conformations per cpu
export group=5 # number of conformations clustered at once
let num_sets=$(($nlconf / $group))
export cluster_num=0

# Start CHARMM
# Loop required to submit subjobs for each cluster group
for ((clust=1; clust<$group; clust+=1))
do
  start_conf=$(((clust*$num_sets)+1))
  end_conf=$(((clust+1)*$num_sets))
  cluster_num=$(($cluster_num+1))
  echo "start conf is:"${start_conf}" and end conf is "${end_conf}

  echo "starting charmm"

  module purge
  module load compilers/intel mpi/openmpi/1.6.1

  mpirun $CHARMM n=${start_conf} lastiter=${end_conf} ligandsegid=${ligand} clustnum=${cluster_num} pconf=${prot_conf} \
        -i 11_cluster2.inp -o output_${start_conf}_${end_conf}
done

# Protocol to submit the final clustering job once all others are complete
# Necessary workaround since the above loop would not submit the final subjob
if [ $end_conf -lt $nlconf ]
then
  end_conf=$(($end_conf+1))
  mpirun $CHARMM n=${start_conf} lastiter=${end_conf} ligandsegid=${ligand} clustnum=${cluster_num} pconf=${prot_conf} \
        -i 11_cluster2.inp -o output_${start_conf}_${end_conf}
fi

echo "end conf is "${end_conf}

# Start charmm
# Final clustering script
mpirun $CHARMM n=${lig_num} ligandsegid=${ligand} pdbid=${protein} pconf=${prot_conf} \
    -i 12_final-clustering.inp -o output_cluster3_${nlconf}

# Initialize varialbes for backmutate step
# Oncoming scripts will need the NEW final number of unique conformations
# produced by the clustering steps
export total_conf=`awk '$1=$1' final_lig_traj_frames.dat`
echo "export total_conf="${total_conf} >> ${vars}
echo "export total_conf="${total_conf} >> ${pldir}/var_${protein}_${prot_conf}_${ligand}
echo "total conf is " ${total_conf}

for (( i=1; i<=$total_conf; i+=1 ))
do
  echo ${i} >> complete_end_ligands.dat
done

# Workaround for getting final ligand conformation number
# when doing only one ligand
#if [ "$batch" -gt "$total_conf" ]
#then
# batch=${total_conf}
# echo "export batch="${batch} >> ${vars}
#fi


# Submit backmutate subjobs
for ((lig_num=1; lig_num<=$batch; lig_num+=1))
do
  sbatch backmutate.slu ${lig_num} ${vars}
done

mv slurm-${jobid}.out ${cluster2_slurm}
