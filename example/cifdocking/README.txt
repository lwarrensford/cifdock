## ARP and LCW

#####   How to Submit   #####

1) Set up your protein
   - these will be protein_confs
   - This is designed for onr proein. It could handle multple proteins, but I don't recommend doing so with a limited batch

2) Set up your ligands
   - make a list of the ligands you want to run in a list called "liglist.dat"
   - you can do as many ligands as you want, just adjust your batch number accordingly

3) cifdock-2.0 should be it's own directory and contain all the scripts within the covid directory. 
   - > MOVE submit-cifdock.sh to your main directory

4) submit from main directory
   		sh submit-cifdock.sh protein protein_number batch
   - protein:
	this is the name of your protein
   - protein_number:
	this is your protein conformation number. Make sure that it matches whatever is in your "proteins conf" folder
   - batch
	your batch will determine how many jobs run at once. It is VERY important to remember how many ligands you 
	are running. Divide your job limit by your number of ligands running and round down 
	- example: if you are running 32 ligands, and your job limit is 250, you'd submit a batch number of 7
		this will run 7 jobs at a time for each ligand for a total of 224 jobs running at once. 
		It will not run more than this number. 
   - example
	sh submit-cifdock.sh tmpr 3 7
	- will submit for the protein tmpr, conformation number 3, in a batch of 7




#####   Organization    #####

1) variable file
   - a directory called vars will be created per protein-ligand combo. In order, this includes:
	- protein name
        - protein conformation number
        - ligand name
        - batch number
        - initial number of ligand poses
        - protein directory path
        - vars file path
        - number of conformations after clustering (generated during cluster2.slu)
   - this file is necessary for these scripts to work properly. Don't mess with it. 
   - this and the ligand conformation number are the only things passed through to the next script
        - this allows you to restart anywhere in the process without having to pass through a 
           ton of variables
   - a copy of this will be in the protein ligand directory, but the file the scripts use is in
      the vars directory

2) slurm files
   - slurm files are automatically sorted into the slurm directory under their respective script name
	- ie, tmpr_3_lig/slurm/backmutate
   - to check a slurm file for a specific ligand pose number, open joblist.dat within the slurm directory.
   	- the ligand number shows its corresponding job id

3) checking the progress of jobs
   - as the jobs run, they write the ligand number to a file called complete_scriptname (ie complete_backmutate)
   - to compare the number of confs with what has been completed, do:
		 wc -l complete_*
   - numbers should match up
	- complete_start_ligands.dat should have the same numbers as:
		- complete_sgld.dat
                - complete_cluster1.dat
	- complete_end_ligands.dat should have the same numbers as:
		- complete_backmutate.dat
		- complete_trajectory.dat
		- complete_energy.dat
	- IF THEY DON'T MATCH UP by the time the jobs have finished, go to the "troubleshooting" section





#####   Troubleshooting   #####

----> both of these take place in the specific protein-ligand directory
----> one of the first things to do is to check your vars file and make sure everything looks good

1) submit-from-start.sh
   - if you need to start a submission script from the beginning:
   	- ie if cluster2.slu finished, but did not submit the next submission script 
   - you will need your script name and your vars path name
	- get your vars path name by opening it's vars file and copying vars=path name
   - submitting:
		sh submit-from-start.sh script-name vars-path
	- example:
		sh submit-from-start.sh backmutate /work/n/name/tmpr/vars/var_tmpr_3_lig
2) submit-missing-ligands.sh
   - use this is some ligands don't complete for some reason
   - it's recommend you check your slurm output to see if there's an easy reason for why the specific ligands
      died (remember to use joblist.dat)
   - so here is where those complete_script-names come in handy. If the number in the complete_script-name.dat 
      doesn't match its respective complete*ligands.dat, then you will use sdiff -s to find the ligand numbers
      that didn't succeed. You will save these numbers into a file called missing.dat
   - submitting:
		sh submit-missing-ligands.sh script-name vars-path
   	- example:
		sdiff -s complete_end_ligands.dat complete_backmutate.dat > missing.dat
 		sh submit-missing-ligands.sh backmutate /work/n/name/tmpr/vars/var_tmpr_3_lig	





######   Script Breakdown   #####

1) submit-cifdock.sh
   - submits one protein conformation at a time
   - will use the "batch" number to submit the first loop of ligand conformations
      to the cluster for each ligand.

2) sgld.slu
   - will conduct sgld sims and loop through the rest of the conformations. Each submission is
      independent of the other jobs running. e.g. job 1 --> submits job 11 --> submits job 21
   - when the loop has reached the number of ligand configurations, will launch the next script 
      e.g. max number is 200, so when it reaches 201, will submit cluster1.slu with 1 as the conf number

 3) cluster1.slu
   - first round of clustering.
   - fill loop like the previous. Instead of resubmitting in a loop,
      will check an output file and wait until everything is finished and then launch cluster2.slu

4) cluster2.slu
   - will finish clustering as one job after cluster1 is fully complete
   - once complete, will launch the next set in the same way submit-cif-dock does with the original batch

5) backmutate, trajectory, and energy will loop through the numbers until done in the same way that 
    sgld.slu does. 

6)scoring.slu
   - will score the poses as one job after energy.slu is complete

