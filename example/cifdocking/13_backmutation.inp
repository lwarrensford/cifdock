* Backmutate mutated Ala residues back to their original residues
*

dimension chsize 200000

PRNLEV  5
WRNLEV -2
BOMLEV -2

! Read topology and parameter files
read rtf card name ../toppar/top_all36_prot.rtf
read rtf card append name ../toppar/top_all36_cgenff.rtf
read rtf card append name ../ligands/@ligandsegid/@ligandsegid.rtf

wrnlev 1
read para card flex name ../toppar/par_all36_prot.prm
read para card append flex name ../toppar/par_all36_cgenff.prm
read para card append flex name ../ligands/@ligandsegid/@ligandsegid.prm
wrnlev 5

stream ../toppar/toppar_water_ions.str
stream ../toppar/patch.str
stream ../ligands/@ligandsegid/@ligandsegid.str

calc numunit = 100 + @N


! Read in protein coordinates
open unit @numunit form read name ../protein_confs/@{pconf}_complex_prep/mutala_origin_protein.psf
read psf card unit @numunit
close unit @numunit

open unit @numunit card read name ../protein_confs/@{pconf}_complex_prep/mutala_origin_protein.cor
read coor card unit @numunit 
close unit @numunit

! Delete waters and ions
define solution select .byres. (resnm TIP3 .or. resnm POT .or. resnm CLA) end
delete atom selec solution end

! Read in ligand coordinates
open unit @numunit form read name backmutate/@ligandsegid_1.psf
read psf card append unit @numunit
close unit @numunit

open unit @numunit card read name backmutate/@ligandsegid_@N.cor
read coor card append unit @numunit
close unit @numunit


CONS FIX SELE NONE END
CONS HARM CLEAR 

ENERGY
INTE SELE SEGID @ligandsegid end SELE .not. SEGID @ligandsegid end
CALC EINTER = ?ENER  !Interaction energy betwen receptor and ligand

IF @EINTER .GT. 9999 THEN
    ! Selection = everything within 5 A of the ligand, but NOT including the ligand
    define activesite sele .byres. (segid @ligandsegid .around. 5.0) .and. .not. segid -
  @ligandsegid end 
    ! Fix ligand and rest of protein (not active site)
    CONS FIX selec .not. activesite end 
    ! Minimize the active site residues
    MINI SD NSTEP 50
    MINI ABNR NSTEP 50 TOLGRA 0.1
    CONS FIX selec none end
    ! Fix everything but the ligand
    CONS FIX selec .not. segid @ligandsegid end
    ! Harmonic restraint on ligand
    CONS HARM BESTFIT MASS FORCE 50.0 select segid @ligandsegid end
    ! Minimize ligand
    MINI SD NSTEP 100
    MINI ABNR NSTEP 100 TOLGRA 0.1

CONS FIX SELE NONE END
CONS HARM CLEAR 
ENDIF
  

set bbselfull = (type C .or. type CA -
         .or. type N .or. type O .or. type H  .or. type HN -
         .or. type CB .or. type HA .or. type HB*) 
  
! Total number of mutated residues
stream ../protein_confs/@{pconf}_complex_prep/residue_list.str
  
set minisd = SD NSTEP 500 NPRINT 50 ! My minimization conditions

set rpn = 0  ! Loops through each mutated residue
set lib = 0  ! Loops through different residue libraries in tuffery format


open unit 14 read form name ../protein_confs/@{pconf}_complex_prep/residue_num
get terminus1 unit 14
increment terminus1 by 0
get terminus_last unit 14
increment terminus_last by 0
  
label next_back_mutation
      incr rpn by 1
      if  @rpn  gt @npos then goto print_pdb_mutate  
! npos is the number of residues to be mutated, the position to change,
! and its segment identifier. Helps keep track 
! of the residues given in residue_list.str 
      set segid = @segid@@rpn
      set res = @res@@rpn ! Gives the exact residue name 
      set resnum = @rd@@rpn

      set isnterm = false
      set iscterm = false
      if resnum eq @terminus1 set isnterm = true
      if resnum eq @{terminus_last} set iscterm = true

      dele atom SELE SEGID @segid .AND. RESID @resnum .AND. .not. @bbselfull end

      if @isnterm eq false then if @iscterm eq false  patch P@res @segid @resnum setup
      if @isnterm eq true   patch N@res @segid @resnum setup
      if @iscterm eq true   patch C@res @segid @resnum setup
      autogenerate angle dihedral
      rename resname @res sele SEGID @segid .AND. resid @resnum end
  
!  goto next_library  ! I am calling the library and looping through all rotamers 
  
! Here the rotamer number is randomized and the correct random phi and psi angles are requested
! one residue at a time
! Here I need to create a loop of residues with correct variables for phi and psi angles, so just COPY the
! subroutine for RES given in rotloop.in and keep ata the end of the script
  
      incr lib by 1
      open unit 40 read form name lib/@res@@lib.lib  ! Here the RES is used as defined in the residue_list.str
      stream readlib.inp
      close unit 40
     
      RANDom uniform scale @rotnum !offset 1  ! The offset here basically moves the scale up by 1
      FORMat (I5)
      echo ?random 
      set rot ?random
      FORMat 
      if rot eq 0 then incr rot by 1 
      set rn = @pos@res@rot

      goto @res       
        
prnlev 5  
label print_pdb_mutate
define missing select .not. hydrogen .and. .not. initialized end
set missingatoms = ?nsel
  
print coor select missing end
IC FILL PRESERVE
IC PARAM
IC BUILD
HBUILD
 

CONS FIX SELE NONE END

! Selection = everything within 9 A of the active site origin and NOT the ligand
define activesite sele .byres. ( point 0. 0. 0. cut 9 ) .and. .not. -
         segid @ligandsegid end
! Harmonic restraint of ligand and protein beyond 11 A
cons harm bestfit force 1000 select .not. activesite end

mini sd nstep 250 nprint 25 tolg 0.0001
mini abnr nstep 100 nprint 25 tolg 0.0001
cons harm clear 
cons fix selec none end

OPEN WRITE CARD UNIT @numunit NAME backmutate/pdbbackmutate@N.cor
WRITE COORDINATES CARD UNIT @numunit
* Initial structures with designed ALA positions
* backmutated to initial residues.

open write unit 29 CARD name backmutate/sgld_@ligandsegid@n_scsampling.res
open write unit 30 file name backmutate/sgld_@ligandsegid@n_scsampling.dcd

! Constraint on ligand and protein beyond 11 A
cons fix selec .not. activesite end
! Set friction forces
SCAL FBETA SET 1.0 SELE activesite END

! 2 ps SGLD of active site residues (11 A beyond active site origin)
DYNA LANG LEAP  STRT  NSTE 1000 TIME 0.002 ECHECK 9E+50 -
    IPRFRQ 100 ISVFRQ 100 IHTFRQ 0 IEQFRQ 0 INBFRQ 10 IHBFRQ 0 -
    IUNREA -1 IUNWRI 29 IUNCRD 30 IUNVEL 33 KUNIT -1 -
    NSAVC 50 NSAVV 0 NPRINT 100 ISEED 314159   -
    SGLD TSGAVG 0.5 TEMPSG 500  TREFLF 10     -
    TBATH 300   FIRST 260 -
    IASORS 0 IASVEL 1 ICHECW 0 -
    NBXMOD 5  ATOM CDIEL SHIFT VATOM VDISTANCE VSWIT -
    CUTNB 12.0  CTOFNB 10.0  CTONNB 8.0  EPS 1.0  E14FAC 1.0  WMIN 1.0


open unit 18 write form name backmutate/interaction_energy_scsampling_waters_ions_@n.dat


OPEN READ UNIT 19 FILE NAME backmutate/sgld_@ligandsegid@n_scsampling.dcd 
TRAJECTORY FIRSTU 19 NUNIT 1 SKIP 1
TRAJECTORY QUERY UNIT 19
SET G 1
SET maxenergy 9999.0
CALC NTOT = ?NFILE


LABEL LOOP1
    TRAJ READ
    UPDATE cutnb 12.0 cutim 12.0 ctofnb 10. ctonnb 8. cdie eps 1.
    ! Interaction energies between protein and ligand only
    inte sele .not. segid @ligandsegid end sele segid @ligandsegid end unit 18
    set etotal1 ?ener
    set evdw1 ?vdw
    set eelec1 ?elec
    IF maxenergy LT ?ENER GOTO NEXT1
       SET frame1 @G
       COOR COPY COMP
       SET maxenergy ?ENER
    LABEL NEXT1
    INCR G BY 1
    IF @G LT @NTOT GOTO LOOP1


read coor file ifile @frame1 name backmutate/sgld_@ligandsegid@n_scsampling.dcd
update CUTNB 12.0  CTOFNB 10.0  CTONNB 8.0 EPS 1.0  E14FAC 1.0  WMIN 1.0
! Calculate interaction energies between protein and ligand
inte sele .not. segid @ligandsegid end sele segid @ligandsegid end
set ETOTAL ?ener
set EVDW ?vdw
set EELEC ?elec

write title unit 18
* ligand @N  frame @frame1  interE @ETOTAL   VdW @EVDW   ELEC @EELEC
*

close unit 18
close unit 19

MINI SD NSTEP 50
MINI ABNR NSTEP 50

cons fix selec none end

! Add back in waters and ions
open unit @numunit form read name ../protein_confs/@{pconf}_complex_prep/waters_init.psf 
read psf card append unit @numunit
close unit @numunit

open unit @numunit card read name ../protein_confs/@{pconf}_complex_prep/waters_init.cor 
read coor card append unit @numunit !resi
close unit @numunit

! If for some reason there are water atoms overlapping with each other once the new waters 
! are added back in, delete them here
delete atom sort sele .byres. (( (resn TIP3 .and. type oh2) .or. resnm POT .or. resnm CLA) .and. - 
  (( .not. (resn TIP3 .or. hydrogen .or. resnm POT .or. resnm CLA)) .around. 2.5 )) show end

! Selection = anything within 11 A of the active site origin
define active select .byres. ( point 0. 0. 0. cut 11) show end
! Delete waters beyond 11 A of the active site
! This reduces computational load on next SGLD step
delete atom select .not. active .and. resn TIP3 show end

open write unit 31 CARD name backmutate/sgld_@ligandsegid@n_backmutate.res
open write unit 32 file name backmutate/sgld_@ligandsegid@n_backmutate.dcd

! Selection = everything within 9 A of active site INLCUDING ligand or waters
define activesite sele .byres. ( point 0. 0. 0. cut 9 ) .or. -
         segid @ligandsegid .or. resn TIP3 show end
! Fix all of the protein BEYOND 9 A of the active site
cons fix selec .not. activesite end
! Set friction forces
SCAL FBETA SET 1.0 SELE activesite END

! 2 ps SGLD of entire active site - side chains, ligand, and waters
DYNA LANG LEAP  STRT  NSTE 1000 TIME 0.002 ECHECK 9E+50 -
    IPRFRQ 100 ISVFRQ 100 IHTFRQ 0 IEQFRQ 0 INBFRQ 10 IHBFRQ 0 -
    IUNREA -1 IUNWRI 31 IUNCRD 32 IUNVEL 33 KUNIT -1 -
    NSAVC 50 NSAVV 0 NPRINT 100 ISEED 314159   - 
    SGLD TSGAVG 0.5 TEMPSG 500  TREFLF 10     -
    TBATH 300   FIRST 260 -
    IASORS 0 IASVEL 1 ICHECW 0 -
    NBXMOD 5  ATOM CDIEL SHIFT VATOM VDISTANCE VSWIT -
    CUTNB 12.0  CTOFNB 10.0  CTONNB 8.0  EPS 1.0  E14FAC 1.0  WMIN 1.0


open unit 21 write form name backmutate/interaction_energy_fullsystem_waters_ions_@n.dat


OPEN READ UNIT 22 FILE NAME backmutate/sgld_@ligandsegid@n_backmutate.dcd 
TRAJECTORY FIRSTU 22 NUNIT 1 SKIP 1
TRAJECTORY QUERY UNIT 22
SET i 1
SET maxenergy 9999.0
CALC NTOT = ?NFILE


LABEL LOOP
    TRAJ READ
    UPDATE cutnb 12.0 cutim 12.0 ctofnb 10. ctonnb 8. cdie eps 1.
    ! Interaction energies between protein and ligand only
    inte sele .not. segid @ligandsegid end sele segid @ligandsegid end unit 21
    set etotal1 ?ener
    set evdw1 ?vdw
    set eelec1 ?elec
    IF maxenergy LT ?ENER GOTO NEXT
       SET frame @i
       COOR COPY COMP
       SET maxenergy ?ENER
    LABEL NEXT
    INCR i BY 1
    IF @i LT @NTOT GOTO LOOP

    read coor file ifile @frame name backmutate/sgld_@ligandsegid@n_backmutate.dcd
    update CUTNB 12.0  CTOFNB 10.0  CTONNB 8.0 EPS 1.0  E14FAC 1.0  WMIN 1.0
    ! Calculate interaction energies between protein and ligand
    inte sele .not. segid @ligandsegid end sele segid @ligandsegid end
    set ETOTAL ?ener
    set EVDW ?vdw
    set EELEC ?elec
    OPEN WRITE CARD UNIT 12 NAME backmutate/pdbbackmutate_premini@N_@frame.pdb 
    WRITE COOR PDb UNIT 12
    * structure with the lowest energy
    * frame number @frame with energy @maxenergy
    *
    
    write title unit 21
    * ligand @N  frame @frame   interE @ETOTAL   VdW @EVDW   ELEC @EELEC
    *
    


MINI SD NSTEP 50
MINI ABNR NSTEP 50
cons fix selec none end


OPEN WRITE CARD UNIT @numunit NAME backmutate/pdbbackmutate_mini@N.cor
WRITE COORDINATES CARD UNIT @numunit
* Initial structures with designed ALA positions
* backmutated to initial residues.

OPEN WRITE CARD UNIT @numunit NAME backmutate/pdbbackmutate_mini@N.psf
WRITE PSF CARD UNIT @numunit
* Initial structures with designed ALA positions
* backmutated to initial residues.

update CUTNB 12.0  CTOFNB 10.0  CTONNB 8.0 EPS 1.0  E14FAC 1.0  WMIN 1.0
! Calculate interaction energies between protein and ligand
inte sele .not. segid @ligandsegid end sele segid @ligandsegid end unit 21
set ETOTAL ?ener
set EVDW ?vdw
set EELEC ?elec

delete atom selec type H* end

OPEN WRITE CARD UNIT @numunit NAME backmutate/pdbbackmutate_mini@N.pdb
WRITE COORDINATES PDB UNIT @numunit
* Initial structures with designed ALA positions
* backmutated to initial residues.



system "echo CONF: @N    ENER: @ETOTAL    VDW:  @EVDW  ELEC:  @EELEC  >> charmmsp_interaction_energy.dat"
!close unit 21
stop



!___________________________________________________

! "Subroutine"  RES 
!-------------------------------------------------------
! Information to build each chosen rotamer.



label GLY

label ALA

label ARG

      ic edit
      dihe @segid @resnum  N  @segid  @resnum CA @segid  @resnum CB @segid @resnum CG @chia@@rn
      dihe @segid @resnum CA  @segid  @resnum CB @segid  @resnum CG @segid @resnum CD @chib@@rn
      dihe @segid @resnum CB  @segid  @resnum CG @segid  @resnum CD @segid @resnum NE @chic@@rn
      dihe @segid @resnum CG  @segid  @resnum CD @segid  @resnum NE @segid @resnum CZ @chid@@rn
      end
      goto build 

label ASN

label ASP

      ic edit
      dihe @segid @resnum N  @segid @resnum CA @segid @resnum CB @segid @resnum CG  @chia@@rn
      dihe @segid @resnum CA @segid @resnum CB @segid @resnum CG @segid @resnum OD1 @chib@@rn
      end
      goto build 

label CYS

      ic edit
      dihe @segid @resnum N @segid @resnum CA @segid @resnum CB @segid @resnum SG @chia@@rn
      end
      goto build 

label GLN

label GLU
      ic edit
      dihe @segid @resnum N  @segid @resnum CA @segid @resnum CB @segid @resnum CG  @chia@@rn
      dihe @segid @resnum CA @segid @resnum CB @segid @resnum CG @segid @resnum CD  @chib@@rn
      dihe @segid @resnum CB @segid @resnum CG @segid @resnum CD @segid @resnum OE1 @chic@@rn
      end
      goto build 

label HIS

label HSE

label HSP 

label HSD

      ic edit
      dihe @segid @resnum N  @segid @resnum CA @segid @resnum CB @segid @resnum CG  @chia@@rn
      dihe @segid @resnum CA @segid @resnum CB @segid @resnum CG @segid @resnum ND1 @chib@@rn
      end
      goto build 

label ILE

      ic edit
      dihe @segid @resnum N  @segid @resnum CA @segid @resnum CB @segid @resnum CG1 @chia@@rn
      dihe @segid @resnum CA @segid @resnum CB @segid @resnum CG1 @segid @resnum CD @chib@@rn
      end
      goto build 

label TYR
label TRP
label LEU
label PHE

      ic edit
      dihe @segid @resnum N  @segid @resnum CA @segid @resnum CB @segid @resnum CG @chia@@rn
      dihe @segid @resnum CA @segid @resnum CB @segid @resnum CG @segid @resnum CD1 @chib@@rn
      end
      goto build 

label LYS

      ic edit
      dihe @segid @resnum N  @segid @resnum CA @segid @resnum CB @segid @resnum CG @chia@@rn
      dihe @segid @resnum CA @segid @resnum CB @segid @resnum CG @segid @resnum CD @chib@@rn
      dihe @segid @resnum CB @segid @resnum CG @segid @resnum CD @segid @resnum CE @chic@@rn
      dihe @segid @resnum CG @segid @resnum CD @segid @resnum CE @segid @resnum NZ @chid@@rn
      end
      goto build 


label MET

      ic edit
      dihe @segid @resnum N  @segid @resnum CA @segid @resnum CB @segid @resnum CG @chia@@rn
      dihe @segid @resnum CA @segid @resnum CB @segid @resnum CG @segid @resnum SD @chib@@rn
      dihe @segid @resnum CB @segid @resnum CG @segid @resnum SD @segid @resnum CE @chic@@rn
      end
      goto build 

label PRO
      !dele atom SELE SEGID @segid .AND. RESID @resnum .AND. TYPE H end
      ic edit
      dihe  @segid @resnum N @segid @resnum CA @segid @resnum CB @segid @resnum CG @chia@@rn
      dihe @segid @resnum CA @segid @resnum CB @segid @resnum CG @segid @resnum CD @chib@@rn
      end
      !quick @segid @resnum N @segid @resnum CA @segid @resnum CB @segid @resnum CG end
      goto build 

label SER

      ic edit
      dihe @segid @resnum N @segid @resnum CA @segid @resnum CB @segid @resnum OG @chia@@rn
      end
      goto build 

label THR

      ic edit
      dihe @segid @resnum N @segid @resnum CA @segid @resnum CB @segid @resnum OG1 @chia@@rn
      end
      goto build 

label VAL

      ic edit
      dihe @segid @resnum N  @segid @resnum CA  @segid @resnum CB  @segid resnum CG1 @chia@@rn
      end
      goto build 
!+++++++++++++++++++++++++++++++++++++++++++

! This is the final step of residue bulding

label build      
      cons fix selec none end
      coor INIT SELE SEGID @segid .AND. RESID @resnum .AND. .NOT. @bbselfull end
      ic purge
      ic para
      ic build
      hbuild

      quick @segid @resnum N @segid @resnum CA @segid @resnum CB @segid @resnum CG 
     ! quick @segid @resnum NH1 @segid @resnum CT1 @segid @resnum CT2 @segid @resnum CA end

! Dihedral using segid/resid/atom ------------
      !quick HHKC 2 N  HHKC 2 CA HHKC 2 CB HHKC 2 CG end
      CONS FIX SELE NONE END
      CONS  FIX SELE .NOT. ( SEGID @segid .AND. RESID @resnum ) show end
      MINI @minisd
      !MINI @miniabnr 
      CONS FIX SELE NONE END    
 
      quick @segid @resnum N @segid @resnum CA @segid @resnum CB @segid @resnum CG 
      goto next_back_mutation
