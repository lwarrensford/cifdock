#### How to Submit

- Submit the docking to the cluster from the main directory
  `sh submit-cifdock.sh [protein] [protein conformation number] [ligand] [batch size]`
   - protein:
     - the name of the protein target
   - protein conformation number:
     - this is your protein conformation number. Make sure that it matches whatever is in your "proteins conf" folder
   - ligand:
     - the name of the ligand that will be docked
   - batch size
     - your batch size will determine how many jobs run at once. Be careful not to set the batch size higher than the 
       total number of initial ligand conformations, otherwise the submission scripts will fail
       - example: if you generated 100 ligands in the ligand preparation step, set your batch size less or equal to 100
          if it is set above 100, not all jobs will submit correctly
   - example
     - `sh submit-cifdock.sh 2acr 1 tol 100`
       - will submit the protein 2ACR, conformation number 1, ligand tolrestat (tol), in a batch size of 100 subjobs

#### Organization 

1. variable file
   - a directory called vars will be created per protein-ligand combo. In order, this includes:
	- protein name
        - protein conformation number
        - ligand name
        - batch number
        - initial number of ligand poses
        - protein directory path
        - vars file path
        - number of conformations after clustering (generated during cluster2.slu)
   - this file is necessary for these scripts to work properly. Avoid editing if necessary. 
   - this and the ligand conformation number are the only things passed through to the next script
        - this allows you to restart anywhere in the process without having to pass through a 
          ton of variables
   - a copy of this will be in the protein ligand directory, but the file the scripts use is in
      the vars directory

2. slurm files
   - slurm files are automatically sorted into the slurm directory under their respective script name
	- example: `2acr_1_tol/slurm/backmutate`
   - to check a slurm file for a specific ligand pose number, open joblist.dat within the slurm directory.
   	- the ligand number shows its corresponding job id

3. checking the progress of jobs
   - as the jobs run, they write the ligand number to a file called complete_scriptname (i.e., complete_backmutate)
   - to compare the number of confs with what has been completed, type in the command line:
     `wc -l complete_*`
   - numbers should match up
	- complete_start_ligands.dat should have the same numbers as:
	  - complete_sgld.dat
          - complete_cluster1.dat
	- complete_end_ligands.dat should have the same numbers as:
          - complete_backmutate.dat
          - complete_trajectory.dat
          - complete_energy.dat
	- if they don't match up by the time the jobs have finished, go to the "troubleshooting" section

#### Troubleshooting 

**Note:** Both of these take place in the specific protein-ligand directory

1. submit-from-start.sh
   - if you need to start a submission script from the beginning:
   	- example: if cluster2.slu finished, but did not submit the next submission script 
   - you will need your script name and your vars path name
	- get your vars path name by opening it's vars file and copying vars=path name
   - submitting:
	`sh submit-from-start.sh [script-name] [vars-path]`
	- example:
		`sh submit-from-start.sh backmutate /work/n/name/2acr/vars/var_2acr_1_tol`
2. submit-missing-ligands.sh
   - use this is some ligands don't complete for some reason
   - it's recommend you check your slurm output to see if there's an easy reason for why the specific ligands
     died (remember to use joblist.dat)
   - so here is where those complete_[script-name] files come in handy. If the number in the complete_[script-name].dat 
     doesn't match its respective complete_ligands.dat, then you will use sdiff -s to find the ligand numbers
     that didn't succeed. You will save these numbers into a file called missing.dat
   - submitting:
	`sh submit-missing-ligands.sh [script-name] [vars-path]`
   	- example:
		`sdiff -s complete_end_ligands.dat complete_backmutate.dat > missing.dat`
 		`sh submit-missing-ligands.sh backmutate /work/n/name/tmpr/vars/var_2acr_1_tol`

#### Script Breakdow 

1. submit-cifdock.sh
   - submits one protein conformation at a time
   - will use the "batch" number to submit the first loop of ligand conformations
     to the cluster for each ligand.

2. sgld.slu
   - will conduct sgld sims and loop through the rest of the conformations. Each submission is
     independent of the other jobs running (e.g., job 1 finishes --> submits job 11 --> submits job 21)
   - when the loop has reached the number of ligand configurations, will launch the next script 
     (e.g., max number is 200, so when it reaches 201, will submit cluster1.slu with 1 as the conf number)

3. cluster1.slu
   - first round of clustering
   - fill loop like the previous. Instead of resubmitting in a loop,
     will check an output file and wait until everything is finished and then launch cluster2.slu

4. cluster2.slu
   - will finish clustering as one job after cluster1 is fully complete
   - once complete, will launch the next set in the same way submit-cif-dock does with the original batch

5. backmutate, trajectory, and energy will loop through the numbers until done in the same way that 
   sgld.slu does 

6. scoring.slu
   - will score the poses as one job after energy.slu is complete

