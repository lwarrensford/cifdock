* Loop through ligand conformation sub-folders and combine the unique dcd files
* Can adjust number of sub-folders clustered per cpu by editing the submission script
* cluster2.slu
*

dimension chsize 1500000

prnlev 5
wrnlev 5
bomlev -2

! Read topology and parameter files
read rtf card name ../toppar/top_all36_prot.rtf
read rtf card append name ../toppar/top_all36_cgenff.rtf
read rtf card append name ../ligands/@ligandsegid/@ligandsegid.rtf

wrnlev 1
read para card flex name ../toppar/par_all36_prot.prm
read para card append flex name ../toppar/par_all36_cgenff.prm
read para card append flex name ../ligands/@ligandsegid/@ligandsegid.prm
wrnlev 5

stream ../toppar/toppar_water_ions.str
stream ../ligands/@ligandsegid/@ligandsegid.str

calc nsnap = 2520/40  ! Number of frames in the trajectory file
set prename mysimulation
set begin 1
set skip  1
set firstiter = @N
set filename @ligandsegid_@firstiter_@lastiter_unique
print @filename

open unit 9 read form name ../ligands/@ligandsegid/number_of_ligand_conf
get nlconf unit 9 
increment nlconf by 0


system "`echo mkdir lig_@FIRSTITER_@LASTITER | awk '{print tolower($0)}'`"
label loop_rmsd_calc
  bomlev -2

! Read in the ligand coordinate file that has active site at the origin
  open unit 10 read form name lig@N/test.psf
  read PSF card unit 10
  close unit 10

  open unit 10 read card name lig@N/test.cor
  read COOR card unit 10
  close unit 10

  cons fix sele none end
  coor copy comp
  coor stats

  calc uone = 1000 + @firstiter
  set miss = 0
  print miss
  open unit 25 write form name dir_list_@firstiter 
  label missing_dcd
      calc inunum = 1000 + @N
      open unform read unit @inunum name lig@N/sgld_unique_poses_@N.dcd 
      IF ?IOSTAT .eq. -1 then  ! subst.doc: 'IOSTAT' - The status of most recent OPEN command -1=failed,1=OK
      incr miss by 1
      incr N by 1
      goto missing_dcd
      ELSE
      !TRAJectory QUERy UNIT @inunum
      close unit @inunum
      write title unit 25
      * lig@N
      incr N by 1
      IF @N .le. @lastiter then goto missing_dcd
      ENDIF 
 
  set I = 1 ! Defining a new variable to get continous unit numbers in appending dcd files
  open unit 11 read form name dir_list_@firstiter 
  get NVAL unit 11          ! The variables @I and @NVAL are different to identify the valid subfolders with data/dcdfiles
  trim NVAL from 4 to 8     ! NVAL is used to identify the sub directories
  increment NVAL by 0
  label combine_dcd
      calc inunum = 1000 + @I
      ! Loop through all the remaining sub folders lig@N and open their respective
      ! dcd files and append them
      open unform read unit @inunum name lig@{NVAL}/sgld_unique_poses_@{NVAL}.dcd
      TRAJectory QUERy UNIT @inunum
      get NVAL unit 11
      trim NVAL from 4 to 8
      increment NVAL by 0
      incr I by 1
      IF @NVAL .NE. 0 then goto combine_dcd
      ENDIF
      close unit 11

  calc num_nunit = @lastiter - @firstiter - @miss + 1
  open unform write unit 900 name final_ligset_@firstiter_@lastiter_sgld_appended.dcd
  ! New trajectory creation
  merge coor firstsu 1001 nunit @{num_nunit} output 900 begin 1 skip 1 NOCHeck 

  write psf card name test.psf
  write coor card name test.cor

  open unform read unit 901 name final_ligset_@firstiter_@lastiter_sgld_appended.dcd
  TRAJectory QUERy UNIT 901
  
  set rstart ?start
  calc rstop = @rstart - ?skip + ?nstep
 

  open unit 902 write form name lig_@firstiter_@lastiter/pairwise_rmsd.rms
  prnl 6

! Create an RMS matrix that gives pairwise RMSDs
  rmsd orient rms norot notran sele segid @ligandsegid end -
       begin @begin skip @skip stop @rstop iwrite 902 -
       iread 901 jread 901 matr

  close unit 901
  close unit 902
  !stop


  envi rms "pairwise_rmsd.rms"
  set dist 0.40
  set maxi 40 

  system "cd lig_@FIRSTITER_@LASTITER; awk '{for (i=1;i<=NF;i++) print $i > FILENAME "." i}' $RMS"
  open unit 11 write form name lig_@firstiter_@lastiter/@{filename}.member
  open unit 12 write form name lig_@firstiter_@lastiter/@{filename}.cst

  correl maxseries @rstop maxtimesteps @rstop 

  set i 1
  label loop_enter
    enter s@i zero
   incr i
   if @i le @rstop goto loop_enter

  set i 1
  label loop_read
    open unit 13 read form name lig_@firstiter_@lastiter/pairwise_rmsd.rms.@i
    READ s@i unit 13 DUMB
    close unit 13
   incr i
   if @i le @rstop goto loop_read

  ! Create the membership and cluster files
  cluster s1 radius @dist MAXCluster 1000 MAXIteration @maxi -
  NFEAture @rstop unicst 12 UNIMember 11

  end
  system "cd lig_@FIRSTITER_@LASTITER; ps -u $USER -o 'vsize,args' >&2"
  system "cd lig_@FIRSTITER_@LASTITER; rm $RMS.[0-9]*"

! Test for the GET and TRIM commands to get the number of clusters from membership file
! Assign to a variable that can be used in charmm

  open unit 14 read form name lig_@firstiter_@lastiter/@{filename}.member
  get line1 unit 14
  get line2 unit 14
  get line3 unit 14
  get line4 unit 14
  get line5 unit 14
  get clusterval unit 14
  trim clusterval from 35 to 38
  increment clusterval by 0
  close unit 14

! Setup for @clusterval clusters and output trajectories

  set C = 1
  label opnloop
  calc u = 500 + @C
  open unit @U write file name lig_@firstiter_@lastiter/final_ligset_@firstiter_@lastiter_sgld_number@C.dcd
  incr C by 1
  if C le @clusterval goto opnloop  ! By looking at the number of clusters formed in unim.txt

! Input files
  open unit 15 read card name lig_@firstiter_@lastiter/@{filename}.member
  open unit 16 read file name final_ligset_@firstiter_@lastiter_sgld_appended.dcd 

  TRAJectory QUERy UNIT 16
  !stop
  prnlev 10

! Split into cluster-based subsets
  merge coor subset firstu 16 nunit 1 outputu 501 nunss @clusterval memssu 15

  close unit 15
  close unit 16 

! Final subroutine of cluster2
! Here we are extracting the first frame from every dcd file in the sub folder and creating
! a new appended trajectory file with just the unique poses
  set I = 0
  open unform write unit 904 name @ligandsegid_@firstiter_@lastiter_final_sgld_clustered_unique_poses.dcd
  traj iwrite 904 nwrite 1 nfile @clusterval skip 1
  label dcd_loop
      incr I by 1
      read coor file ifile 1 NAME lig_@firstiter_@lastiter/final_ligset_@firstiter_@lastiter_sgld_number@i.dcd 
      TRAJ write
      IF I .LT. @clusterval goto dcd_loop

  open unform read unit 911 name @ligandsegid_@firstiter_@lastiter_final_sgld_clustered_unique_poses.dcd 
  TRAJ QUERY UNIT 911


stop

 
