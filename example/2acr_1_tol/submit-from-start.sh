#!/bin/bash
## Submit a submission script from the beginning 

export script=${1}
export vars=${2}

source ${vars}
cat ${vars}

if [ "$script" = "cluster2" ] || [ "$script" = "scoring" ]
 then
   sbatch ${script}.slu ${vars}
 else
   for (( lig_num=1; lig_num<=$batch; lig_num+=1 ))
    do
     sbatch ${script}.slu ${lig_num} ${vars}
   done
fi
