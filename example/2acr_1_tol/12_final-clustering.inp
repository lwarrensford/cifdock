* This is the final script in the clustering series, it takes all the
* final dcd files from cluster2, merges them and then clusters to produce unique
* ligand conformations within the protein activesite.
*

dimension chsize 1500000

prnlev 5
wrnlev 5
bomlev -2

! Read topology and parameter files
read rtf card name ../toppar/top_all36_prot.rtf
read rtf card append name ../toppar/top_all36_cgenff.rtf
read rtf card append name ../ligands/@ligandsegid/@ligandsegid.rtf

wrnlev 1
read para card flex name ../toppar/par_all36_prot.prm
read para card append flex name ../toppar/par_all36_cgenff.prm
read para card append flex name ../ligands/@ligandsegid/@ligandsegid.prm
wrnlev 5

stream ../toppar/toppar_water_ions.str
stream ../ligands/@ligandsegid/@ligandsegid.str

! Here the variable @rstop will set the number of frames used for clustering
! and it should be less than 1000 for clustering to work efficiently. If >1000 
! perform this 3rd round of clustering two times by breaking the number of dcd 
! list file into halves on parallel processors 
set prename mysimulation
set begin 1
set skip  1
set filename cluster3_unique
!print filename

open unit 9 read form name ../ligands/@ligandsegid/number_of_ligand_conf
get nlconf unit 9
increment nlconf by 0


open unit 10 read form name test.psf
read PSF card unit 10
close unit 10

open unit 10 read card name test.cor
read COOR card unit 10
close unit 10

system "`echo ls @LIGANDSEGID_[0-9]*_[0-9]*_final_sgld_clustered_unique_poses.dcd | awk '{print tolower($0)}'` > cluster2_dcd_list"

open unit 11 read form name "cluster2_dcd_list"
get dcd_name unit 11
set N = 1

label combine_dcd
    calc inunum = 100 + @N
    open unform read unit @inunum name @{dcd_name}
    TRAJectory QUERy UNIT @inunum
    incr N by 1
    get dcd_name unit 11
    if @{dcd_name} .ne. END-OF-FILE then goto combine_dcd

calc numunit = @N -1
open unform write unit 900 name cluster3_sgld_uniquepose_combined_traj.dcd
! New trajectory creation
merge coor sele all end -
        first 101 nunit @numunit begin @begin skip @skip output 900 NOCHeck

open unit 901 unform read name cluster3_sgld_uniquepose_combined_traj.dcd 
open unit 902 write form name cluster3_pairwise_rmsd.rms  !RMSD matrix

! It would be helpful to use a variable inside the ENVI command to loop
! through different values
TRAJectory QUERy UNIT 901
set rstart ?start
calc rstop = @rstart - ?skip + ?nstep
set skip ?skip
set begin 1
! If there are more than 1000 ligand conformations, this if statement
! keeps the job running
IF @rstop .GT. 999 then
     set rstop 999 
ENDIF

prnlev 6

! Here we are creating an RMS matrix that gives pairwise RMSDs
rmsd orient rms norot notran sele segid @ligandsegid end -
     begin @begin skip @skip stop @rstop iwrite 902 -
     iread 901 jread 901 matr

close unit 901
close unit 902

envi rms "cluster3_pairwise_rmsd.rms"
set dist 0.27
set maxi 40

system "awk '{for (i=1;i<=NF;i++) print $i > FILENAME "." i}' $RMS"
open unit 11 write form name @filename.member
open unit 12 write form name @filename.cst

correl maxseries @rstop maxtimesteps @rstop

set i 1
label loop_enter
  enter s@i zero
 incr i
 if @i le @rstop goto loop_enter

set i 1
label loop_read
  open unit 13 read form name cluster3_pairwise_rmsd.rms.@i
  READ s@i unit 13 DUMB
  close unit 13
 incr i
 if @i le @rstop goto loop_read

! Here we are creating the membership file as well as cluster file
cluster s1 radius @dist MAXCluster 1000 MAXIteration @maxi -
NFEAture @rstop unicst 12 UNIMember 11

end
system "ps -u $USER -o 'vsize,args' >&2"
system "rm $RMS.[0-9]*"

close unit 11
close unit 12

! Test for the GET and TRIM commands to get the number of clusters from
! membership file
! Assign to a variable that can be used in charmm

open unit 14 read form name @filename.member
get line1 unit 14
get line2 unit 14
get line3 unit 14
get line4 unit 14
get line5 unit 14
get clusterval unit 14
trim clusterval from 35 to 38
increment clusterval by 0
close unit 14

! Setup for @clusterval clusters and output trajectories

set C = 1
label opnloop
calc u = 1500 + @C
!open unit @U write file name sgld_@{ligandsegid}@N_cluster_number@C.dcd
open unit @U write file name cluster3_sgld_subcluster@C.dcd 
incr C by 1
if C le @clusterval goto opnloop  ! By looking at the number of clusters formed in unim.txt

! Input files
open unit 15 read card name @filename.member
open unit 16 read file name cluster3_sgld_uniquepose_combined_traj.dcd 

TRAJectory QUERy UNIT 16
prnlev 10

! Split into cluster based subsets
merge coor subset firstu 16 nunit 1 outputu 1501 nunss @clusterval memssu 15 -
      stop @rstop

close unit 15
close unit 16

! Here we are extracting the first frame from every dcd file in the sub
! folder and creating a new appended trajectory file with just the unique poses
set I = 1

open unform write unit 904 name final_sgld_uniquepose_combined_traj.dcd 
traj iwrite 904 nwrite 1 nfile @clusterval skip 1
label dcd_loop
    read coor file ifile 1 NAME cluster3_sgld_subcluster@i.dcd 
    TRAJ write
    incr i by 1
    if i LE @clusterval goto dcd_loop

! This is the last sub script in this series
! Here I am separating each ligand file as separate psf and cor file
open unform read unit 911 name final_sgld_uniquepose_combined_traj.dcd 
TRAJ QUERY UNIT 911
set rstart ?start
calc rstop = @rstart - ?skip + ?nstep
open unit 18 write form name final_lig_traj_frames.dat
write title unit 18
* ?nstep 
*
close unit 18

! Make new directory for next step - backmutation
system "mkdir backmutate"

TRAJectory firstsu 911 nunit 1 skip ?skip begin ?start stop @rstop

! Place the new separate psf and cor files for each conformation in the
! backmutation directory to prepare for backmutation
set I 1
calc num1unit = 5000 + @I
write psf card name backmutate/@{ligandsegid}_@I.psf unit @num1unit
* PSF
*
label loop_traj
  TRAJ read
  calc num1unit = 5000 + @I
  write coor card name backmutate/@ligandsegid_@I.cor name unit @num1unit
  * COOR
  *
  incr I by 1
  if @I LE @rstop goto loop_traj



stop

