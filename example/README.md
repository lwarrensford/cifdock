# CIFDock Example Directory

In this directory, you will find an exact clone of the files necessary to run CIFDock, 
as well as a subdirectory (2acr_1_tol/) of a test run of the workflow. In this subdirectory 
you can examine the final output of a standard CIFDock run, where we have docked the molecule 
tolrestat (tol) into aldose reductase (PDB ID: 2ACR). If you would like to run the test example 
yourself, please follow the readmes in the subdirectories (Wiki coming soon!) which will guide 
you through the steps. 

**Note:** This example was run using the old submission procedure. CIFDock uses GNU Parallel
to submit subjobs now. If you'd like to use the old submission procedure used in this example, 
please see the README in the cifdocking/ directory.

Feel free to look through all of the files in this directory to get an idea of what CIFDock 
should look like upon completion of a successful run. Below are some highlights:

1. protein_confs/1_complex_prep/
   - This subdirectory contains the structure of the target protein, 2ACR, built in CHARMM 
     with active site residues mutated to Alanine. 
   - The target was built from an initial structure taken from the PDB, so this example 
     represents the use of scripts 1-6 with corresponding outputs.
   - The target protein in this example, 2ACR, is unique in that it has a co-factor, NADPH, 
     that is necessary during the docking procedure. A co-factor may not always be present 
     in a docking simulation, but it is represented in this example.

2. ligands/tol/
   - This subdirectory contains the structure of the ligand that is docked during the 
     example, tolrestat. 
   - It was in this subdirectory that conformations of the ligand were generated with 
     Confab, of which you will find CHARMM psf/cor files for each here. 

3. 2acr_1_tol/
   - This subdirectory contains all of the files from the automated docking procedure. 
   - The primary job was submitted to a computing cluster that uses SLURM to handle 
     job/subjob submission, so you will find submission scripts designed for such a 
     cluster.
     - If your computing cluster uses different scheduling software, it is recommended 
       to adjust the submission scripts (*.slu files). See the README for more info.
   - The final output poses from the example docking can be found here.
     - The PDB files of these poses are in the final_analysis/ directory.
     - Numbering of the PDB files is not representative of the rank or score of a pose, 
       the number merely reflects an arbitrary number assigned to each ligand conformation.
   - The score calculated for the top five final poses is summarized in the file 
     `top_5_self_made_scoring_function_ranking.dat`.
     - The file is divided into five uniquely numbered columns that represent different 
       custom scoring functions.
     **Note:** Information on the custom scoring functions can be found in the CIFDock paper.
     - Each column lists the top five poses scored by the individual scoring function, along 
       with the pose number (which identifies the corresponding PDB file) and the raw 
       score calculated from that scoring function. 

4. vars/
   - This subdirectory simply saves the many variables necessary for each docking. In 
     general it is best practice to avoid editing these, and instead using them to ensure 
     that all of the correct variables are being passed during the docking run.
   - If there are errors in these files, look to the troubleshooting section of the 
     cifdocking/ readme. 
