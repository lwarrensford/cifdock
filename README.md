# CIFDock: CHARMM-based Induced Fit Docking

CIFDock is a fully CHARMM-based induced fit docking protocol. Our goal is to make fully flexible 
molecular docking available to all. We accomplish this through the use of freely-available 
computational software written and packaged into an easy-to-use induced fit docking workflow.
CIFDock utilizes CHARMM-based self-guided Langevin dynamics, Confab ligand conformational 
sampling, and select active site alanine mutations to simulate the induced fit effect during 
the molecular docking procedure. More detailed information on the background, theory, and 
specific methods can be found in the corresponding publication: [Journal of Computational Chemistry, 2021](https://doi.org/10.1002/jcc.26759).

The entire procedure is run via the command line, and was designed specifically to be run 
on a computing cluster. This was done in order to handle the large amount of subjobs that 
are run in parallel during the docking procedure. However, the workflow 
can be trivially adjusted to run the docking procedure interactively. The full workflow consists 
of the three main steps listed below. More detailed information on each 
step can be found in the corresponding sub-directories. Installation requirements and a 
detailed overview of running CIFDock can be found in the main directory.

#### Protein preparation

This initial step involves building the target protein model in CHARMM. You must have 
either a crystal structure or homology model of the desired target. A series of CHARMM scripts 
must then be run interactively to build the CHARMM structure and coordinate files.

#### Ligand preparation

This step involves generating a series of ligand conformations by using Confab to 
sample rotatable bonds of the ligand molecule. These ligand conformations will be used as 
initial starting conformations during the docking procedure, and serve to enhance ligand 
conformational space sampling.

#### Docking

The main induced fit docking procedure. Takes the newly built protein and ligand models 
and runs a series of CHARMM scripts to dock the ligand, backmutate, and sample the complex. 
This part of the procedure is entirely automated, and is designed to be submitted to and run 
on a computing cluster. Outputs final docked poses as PDBs along with a score calculated 
from a set of custom-developed scoring functions.

Please contact [Luke Warrensford](mailto:lwarrensford@usf.edu) or [Rose Pittman](mailto:pra@usf.edu) for support.
Special thanks to Sai Vankayala, Fiona Kearns, and Ben Pollard!

---------------------------
CIFDock<br>
Woodcock Lab<br>
University of South Florida  
